﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcGrad2
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c, d;
            double x1 = 0, x2 = 0;
            Console.WriteLine("Introduceti a: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti b: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti c: ");
            c = int.Parse(Console.ReadLine());

            if (a==0)
            {
                Console.WriteLine("Ecuatia " + a + "x^2 + " + b + "x + " + c + " = 0 nu are solutii");
                Console.ReadKey();
                return;
            }
            d = b * b - (4 * a * c);
            Console.WriteLine("Delta = " + d);
            if (d < 0)
            {
                Console.WriteLine("Ecuatia " + a + "x^2 + " + b + "x + " + c + " = 0 nu are solutii reale");
                Console.ReadKey();
                return;
            }

            else
                if (d == 0)
            {
                x1 = x2 = -b / 2 * a;
            }
            else
            {
                x1 = (-b + Math.Sqrt(d)) / 2 * a;
                x2 = (-b - Math.Sqrt(d)) / 2 * a;
            }
            Console.WriteLine("Ecuatia " + a + "x^2 + " + b + "x + " + c + " = 0 are solutiile: ");
            Console.WriteLine("X1 = "+x1);
            Console.WriteLine("X2 = "+x2);
            Console.ReadKey();
        }
        
    }
}
